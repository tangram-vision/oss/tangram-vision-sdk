//! Sample program for generating the object-space from a Checkerboard / ChArUco target field
//! description.
//!

use anyhow::Result;
use getopts::Options;
use opencv::{aruco::PREDEFINED_DICTIONARY_NAME, prelude::*};

fn main() -> Result<()> {
    let args: Vec<String> = std::env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();

    opts.optflag("h", "help", "Print arguments to console.")
        .reqopt(
            "w",
            "width",
            "Width (number of squares horizontally) in the checkerboard grid.",
            "<int>",
        )
        .reqopt(
            "H",
            "height",
            "Height (number of squares vertically) in the checkerboard grid.",
            "<int>",
        )
        .reqopt(
            "e",
            "edge-length",
            "Length of the edge of each square in the grid.",
            "<float>",
        )
        .optopt(
            "m",
            "marker-length",
            "Length of the edge of ArUco markers in grid, if using ChArUco.",
            "<float>",
        )
        .optopt(
            "v",
            "variance",
            "Variance along each object-space point dimension.",
            "<float>",
        );

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(_) => {
            let brief = format!("Usage: {} [options]", program);
            println!("{}", opts.usage(&brief));
            std::process::exit(1);
        }
    };

    if matches.opt_present("h") || args.len() == 1 {
        let brief = format!("Usage: {} [options]", program);
        println!("{}", opts.usage(&brief));
        std::process::exit(1);
    }

    let grid_dimensions = {
        let width = matches.opt_get::<i32>("width")?.unwrap();
        let height = matches.opt_get::<i32>("height")?.unwrap();

        (width, height)
    };

    let edge_length = matches.opt_get::<f32>("edge-length")?.unwrap();

    let marker_length = if matches.opt_present("m") {
        matches.opt_get::<f32>("marker-length")?.unwrap()
    } else {
        // Make the length of these 85% of the edge length by default if not provided.
        0.85 * edge_length
    };

    let dictionary = PREDEFINED_DICTIONARY_NAME::DICT_6X6_250;

    let variances = if matches.opt_present("v") {
        let v = matches.opt_get::<f32>("variance")?.unwrap();
        (v, v, v)
    } else {
        // Take 5% of the edge length as the std. deviation
        let v = (0.05f32 * edge_length).powi(2);
        (v, v, v)
    };

    write_charuco_board(
        grid_dimensions,
        edge_length,
        marker_length,
        dictionary,
        variances,
    )
}

/// Given the dimensions, lengths, etc. writes out the checkerboard coordinates.
fn write_charuco_board(
    grid_dimensions: (i32, i32),
    edge_length: f32,
    marker_length: f32,
    dictionary: PREDEFINED_DICTIONARY_NAME,
    variances: (f32, f32, f32),
) -> Result<()> {
    let (width, height) = grid_dimensions;
    let dict = opencv::aruco::get_predefined_dictionary(dictionary)?;

    let (vx, vy, vz) = variances;

    let mut board =
        opencv::aruco::CharucoBoard::create(width, height, edge_length, marker_length, &dict)?;

    let mut writer = csv::Writer::from_writer(std::io::stdout());
    writer.write_record(&[
        "ID",
        "Point X",
        "Point Y",
        "Point Z",
        "Variance X",
        "Variance Y",
        "Variance Z",
    ])?;

    for (id, point) in board.chessboard_corners().iter().enumerate() {
        writer.write_record(&[
            id.to_string(),
            point.x.to_string(),
            point.y.to_string(),
            point.z.to_string(),
            vx.to_string(),
            vy.to_string(),
            vz.to_string(),
        ])?;
    }

    writer.flush()?;
    Ok(())
}
