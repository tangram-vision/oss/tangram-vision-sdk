# Examples

Look at the source code of the examples to find hepful comments and pointers
about how to get the most out of the Tangram Vision SDK. Run any of the examples
here using `cargo`:

```bash
$ cargo run --example <demo name>
```

## SDK Basics

- [build_your_own_plex](build_your_own_plex.rs): Demonstrates how to use the
  Plex APIs in the SDK to create a Plex to represent your multi-sensor system.

## Calibration

- [generate_checker_targets](generate_checker_targets.rs): This example shows
  how to generate an appropriate object-space definition for use with Tangram
  Vision's calibration sytem. While it doesn't use any of the SDK APIs, it is a
  useful tool in getting the object coordinates of your checker / ChArUco grid.
